function init() {
    //Notification.requestPermission();
    firebase.auth().onAuthStateChanged((user) => {
        if (user) {
            var name = user.displayName;
            $("#profile").html(
                "<img src='"+ user.photoURL +"' class='profileIcon mr-2'>" + "<a href='profile.html' class='profile'>" + name + "</a> <a href='#' class='Login' onclick='Logout()'>登出</a>"
            );

            var nameHtml = "<div class='left'><label class='mr-3'>使用者名稱:</label></div><div class='right'><input class='userNameInput' id='name' value='"+ name +"'></div>";
            var emailHtml = "<div class='left'><label class='mr-3'>電子信箱:</label></div><div class='right'><text class='email'>" + user.email + "</text></div>";
            var passwordHtml = "<div class='left'><label class='mr-3'>新的密碼:</label></div><div class='right'><input type='password' class='passwordInput' id='password' value='' ></div>";
            var passwordHtml_ = "<div class='left'><label class='mr-3'>確認密碼:</label></div><div class='right'><input type='password' class='passwordInput' id='password_' value='' ></div>";
            var imgHtml = "<div class='mt-3'><input type='file' accept='image/*' id='photoUp' class='photoInput'><label for='photoUp'><img src='"+ user.photoURL +"' class='photo' id='photo'></label></div>"
            var submitHtml = "<input type='button' value='提交' class='submitBtn' id='submit'>";

            $("#content").html(nameHtml + emailHtml + passwordHtml + passwordHtml_ + imgHtml + submitHtml);

            $("#photoUp").change(()=>{
                var selectedFile = $('#photoUp')[0].files[0];
                if(selectedFile){
                    var reader = new FileReader();
                    reader.onload = e=>{
                        $("#photo").attr("src",e.target.result);
                    }
                    reader.readAsDataURL(selectedFile);
                }
            })

            $("#submit").click(()=>{
                var profileRef = firebase.database().ref("user/profile").orderByChild("email").equalTo(user.email);
                
                var password = $("#password").val();
                var password_ = $("#password_").val();
                var updatePW = checkPrassword(password, password_);

                var photo = $("#photo").attr("src");

                var userName = $("#name").val();

                if(updatePW != -1){
                    loading(true);
                    if(updatePW == 1){
                        user.updatePassword(password);
                    }

                    if(userName != name && photo != user.photoURL){
                        //update name and photo
                        profileRef.once("value").then(snp=>{
                            $.each(snp.val(),(i,item)=>{
                                if(userName != name){
                                    firebase.database().ref("user/profile/").once("value").then(result=>{
                                        var exists = false;
                                        $.each(result.val(),(i_,item_)=>{
                                            if(userName == item_.name){
                                                exists = true;
                                                return false;
                                            }
                                        })
                                        if(!exists){
                                            firebase.database().ref("user/profile/" + i).update({name:userName});
                                            user.updateProfile({displayName:userName}).then(()=>{
                                                if(photo != user.photoURL){
                                                    var selectedFile = $('#photoUp')[0].files[0];
                                                    var storagePhotoRef = firebase.storage().ref("img/"+item.photo);
                                                    
                                                    storagePhotoRef.put(selectedFile).then(()=>{
                                                        storagePhotoRef.getDownloadURL().then(url=>{
                                                            user.updateProfile({photoURL:url}).then(()=>{
                                                                updateSuccess();
                                                                loading(false);
                                                            });
                                                        })
                                                    }).catch(e=>{
                                                        alert(e.message);
                                                        loading(false);
                                                    })
                                                }
                                            })
                                        }else{
                                            alert("名字已存在");
                                            loading(false);
                                        }
                                    }).catch(e=>{
                                        alert(e.message);
                                        loading(false);
                                    })
                                }
                            })
                        }).catch(e=>{
                            alert(e.message);
                            loading(false);
                        })
                    }else if(userName != name){
                        profileRef.once("value").then(snp=>{
                            $.each(snp.val(),(i,item)=>{
                                if(userName != name){
                                    firebase.database().ref("user/profile/").once("value").then(result=>{
                                        var exists = false;
                                        $.each(result.val(),(i_,item_)=>{
                                            if(userName == item_.name){
                                                exists = true;
                                                return false;
                                            }
                                        })
                                        if(!exists){
                                            firebase.database().ref("user/profile/" + i).update({name:userName});
                                            user.updateProfile({displayName:userName}).then(()=>{
                                                loading(false);
                                                updateSuccess();
                                            }).catch(e=>{
                                                alert(e.message);
                                                loading(false);
                                            })
                                        }else{
                                            alert("名字已存在");
                                            loading(false);
                                        }
                                    }).catch(e=>{
                                        alert(e.message);
                                        loading(false);
                                    })
                                }
                            })
                        }).catch(e=>{
                            alert(e.message);
                            loading(false);
                        })
                    }
                    else if(photo != user.photoURL){
                        profileRef.once("value").then(snp=>{
                            $.each(snp.val(),(i,item)=>{
                                if(photo != user.photoURL){
                                    var selectedFile = $('#photoUp')[0].files[0];
                                    var storagePhotoRef = firebase.storage().ref("img/"+item.photo);
                                    
                                    storagePhotoRef.put(selectedFile).then(()=>{
                                        storagePhotoRef.getDownloadURL().then(url=>{
                                            user.updateProfile({photoURL:url}).then(()=>{
                                                loading(false);
                                                updateSuccess();
                                            });
                                        }).catch(e=>{
                                            alert(e.message);
                                            loading(false);
                                        })
                                    }).catch(e=>{
                                        alert(e.message);
                                        loading(false);
                                    })
                                }
                            })
                        }).catch(e=>{
                            alert(e.message);
                            loading(false);
                        })
                    }
                    else {
                        updateSuccess();
                        loading(false);
                    }
                }
            })

        } else {
            $("#profile").html(
                '<a href="signPage.html" class="Login">登入</a>'
            )
        }
    })

}

function checkPrassword(password, password_){
    if(password == ""){
        return 0;
    }
    else if(password.length < 6){
        alert("密碼長度不能少於6");
        return -1;
    }
    else if(password_ != password){
        alert("密碼不一致");
        return -1;
    }else{
        return 1;
    }
}

function Logout() {
    firebase.auth().signOut().then(function () {
        window.location.href = "index.html";
    })
}

function updateSuccess(){
    alert("更新成功");
    window.location.href = "index.html";
}

function loading(state){
    if(state == true){
        $("#loading").css("display","block");
    }else{
        $("#loading").css("display","none");
    }
}