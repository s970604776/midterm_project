
function init() {
    firebase.auth().onAuthStateChanged((user) => {
        if (user) {
            Notification.requestPermission();

            checkYourComment(user.email);

            var name = user.displayName;

            $("#profile").html(
                "<img src='"+ user.photoURL +"' class='profileIcon mr-2'>" + "<a href='profile.html' class='profile'>" + name + "</a> <a href='#' class='Login' onclick='Logout()'>登出</a>"
            );
        } else {
            $("#profile").html(
                '<a href="signPage.html" class="Login">登入</a>'
            )
        }
    })
    init_post();
}

function init_post(pageNum = 1){
    var refStr =  $("#id").html();
    let paramater = 15;

    if(refStr){
        var getNumChildrenRef = firebase.database().ref("poster/"+refStr);
        var posterRef = firebase.database().ref("poster/"+refStr).limitToLast(paramater * pageNum);
    
        var poster = $("#poster");
        loading(true);

        poster.html("");
        if(refStr=="life") $("#header").text("生活時事");
        else if(refStr=="love") $("#header").text("愛情");
        else if(refStr=="knowledge") $("#header").text("學術");
        else if(refStr=="game") $("#header").text("遊戲");
        
        getNumChildrenRef.once("value").then(result=>{
            var num = result.numChildren();
            posterRef.once("value").then(snp=>{
                var counter = 0;
                $.each(snp.val(), (i,item)=>{
                    var key = Object.keys(snp.val())[counter++];
                    //createViewPoster
                    {
                        var text = "<poster class='mt-1 mb-1' onclick='openSubPoster(`"+ key +"`,"+pageNum+")'><p class='eachTitle mt-2 mb-1' id='" + key + "'></p></poster>";
                        poster.prepend(text);

                        var subPoster = $(`#${key}`);
                    
                        subPoster.text('・' + item.title);
                    }
    
                    if(counter==paramater) return false;
                })
                poster.html("<button id='newPost_btn' class='newPost_btn mb-4'>新增貼文</button>"+poster.html());
                var newPost_btn = $("#newPost_btn");
                newPost_btn.click(newPost);
                
                var backpage = "<label class='nextpage' id='backpage'>上一頁</label>";
                if(pageNum == 1) backpage = "";
                var nextpage = "<label class='nextpage ml-2' id='nextpage'>下一頁</label>";
                if(pageNum >= Math.ceil(num/paramater)) nextpage = "";

                var inputNum = "<input type='number' class='inputNum ml-1' id='inputNum' value="+ pageNum +">"

                var pageControler = "<div class='mt-3 mb-3'>"+ backpage + inputNum + "/ "+  Math.ceil(num/paramater) + nextpage +"</div>";
                poster.append(pageControler);

                $("#backpage").click(()=>{
                    pageNum = pageNum - 1;
                    init_post(pageNum);
                })

                $("#nextpage").click(()=>{
                    pageNum = pageNum + 1;
                    init_post(pageNum);
                })
                $("#inputNum").change(()=>{
                    if($("#inputNum").val() == "" || $("#inputNum").val() < 1 || $("#inputNum").val() > Math.ceil(num/paramater)){
                        $("#inputNum").val(pageNum);
                    }else{
                        init_post(Math.floor($("#inputNum").val()));
                    }
                })

                loading(false);

            }).catch(e=>{
                alert(e.message);
                loading(false);
            })
        }).catch(e=>{
            alert(e.message);
            loading(false);
        })
    }
}

function newPost(){
    var poster = $("#poster");
    var refStr =  $("#id").html();

    poster.html(
        "<input type='text' id='postTitle' class='postTitle mb-3' placeholder='標題'>" + 
        "<textarea class='postContent mb-2' id='postContent' placeholder='內容'></textarea>" +
        "<button class='newPost_btn mr-2' id='submit_btn'>提交</button>" +
        "<button class='newPost_btn' id='back_btn'>返回</button>"
    )
    $("#back_btn").click(()=>{
        window.location.href = refStr + ".html";
    })
    $("#submit_btn").click(()=>{
        var user = firebase.auth().currentUser;
        var refStr =  $("#id").html();

        if(!user){
            alert("請登入");
            window.location.href = refStr + ".html";
        }else{
            var auth = user.email;
            var content = $("#postContent");
            var title = $("#postTitle");
            var posterRef = firebase.database().ref("poster/"+refStr);
    
            if(!title.val() || !content.val()){
                alert("標題或內容不能空白");
            }else{
                var date = new Date();
    
                var p = {
                    auth: auth,
                    title: title.val(),
                    content: content.val(),
                    date: date.getFullYear().toString() + "-" + (date.getMonth() + 1) + "-" + date.getDate().toString() + " " + date.getHours().toString() + ":" + date.getMinutes().toString()
                }
    
                posterRef.push(p);
                alert("貼文已送出");
    
                window.location.href = refStr + ".html";
            }
        }
    })
}

function openSubPoster(key,pageNum){
    
    var posterRef = firebase.database().ref("poster/"+ $("#id").html() +"/"+ key);
    posterRef.once("value").then(snp=>{
        var authRef = firebase.database().ref("user/profile").orderByChild("email").equalTo(snp.val().auth);
        authRef.once("value").then(authSnp=>{
            var Auth, photo;
            $.each(authSnp.val(),(i,item)=>{
                Auth = item.name;
                photo = item.photo;
            })

            var poster = $("#poster");

            var define_icon = "https://firebasestorage.googleapis.com/v0/b/ness-forum.appspot.com/o/img%2Fdefine_icon.png?alt=media&token=cfc482a5-8c88-4c0d-a5c0-2bcde0886be7";
            var f0_img = "<img class='posterIcon' id='posterIcon' src='"+define_icon+"'>"
            var f0_auth = "<div class='authBar'>"+f0_img+"<label class='auth'><b>"+ Auth +"</b></label></div>";
            var f0_date = "<label class='date'>"+ "#0 " + snp.val().date + "</label>";
            var f0_content = "<label class='content' id='content'></label>";
            var commentBtn = "<button class='newPost_btn mb-1' style='float:right' id='comment'>回覆</button>";
            var backBtn = "<button class='newPost_btn mb-1' id='back_btn'>返回</button>"
            var f0 =  backBtn + commentBtn + "<div class='block mb-4'>"+ f0_auth + f0_date + f0_content +"</div>";

            poster.html(f0);
            $("#header").text(snp.val().title);

            $("#content").text(snp.val().content);
            $("#content").html($("#content").html().replace(/\n/g,"<br>"));

            var photoStr = "img/"+photo;
            var storageRef = firebase.storage().ref(photoStr);

            storageRef.getDownloadURL().then(url=>{
                $("#posterIcon").attr("src",url);
            })

            //generate btn
            {
                $("#back_btn").click(()=>{
                    init_post(pageNum);
                })
                $("#comment").click(()=>{
                    poster.html(
                        "<textarea class='postContent mb-2' id='postContent' placeholder='內容'></textarea>" +
                        "<button class='newPost_btn mr-2' id='submit_btn'>提交</button>" +
                        "<button class='newPost_btn' id='back_btn'>返回</button>"
                    )
                    $("#back_btn").click(()=>{
                        openSubPoster(key,pageNum);
                    })
                    $("#submit_btn").click(()=>{
                        var user = firebase.auth().currentUser;
                        if(!user){
                            alert("請登入");
                            openSubPoster(key,pageNum);
                        }else{
                            loading(true);
                            var auth = user.email;
                            var content = $("#postContent");
                            var ref = posterRef.child("comment");
                            
                            if(!content.val()){
                                alert("內容不能空白");
                            }else{
                                var date = new Date();
                    
                                var p = {
                                    auth: auth,
                                    content: content.val(),
                                    date: date.getFullYear().toString() + "-" + (date.getMonth() + 1) + "-" + date.getDate().toString() + " " + date.getHours().toString() + ":" + date.getMinutes().toString()
                                }
                                var key2 = Object.keys(authSnp.val())[0];
                                ref.push(p,()=>{
                                    var r = {
                                        title: snp.val().title
                                    }
                                    firebase.database().ref("user/profile/"+key2+"/poster").once("value").then(snp_=>{
                                        var exists = false;
                                        $.each(snp_.val(),(i,item)=>{
                                            if(item.title==snp.val().title) exists=true;
                                        })
                                        if(!exists){
                                            firebase.database().ref("user/profile/"+key2+"/poster").push(r,()=>{
                                                loading(false); 
                                                alert("留言成功");
                                                openSubPoster(key,pageNum);
                                            })
                                        }else{
                                            loading(false); 
                                            alert("留言成功");
                                            openSubPoster(key,pageNum);
                                        }

                                    }).catch(e=>{
                                        loading(false); 
                                        alert(e.message);
                                    })
                                });
                            }
                        }
                    })
                })
            }

            var counter = 1;
            var auth = [];

            //comment
            $.each(snp.val().comment,(i,item)=>{ 
                auth[counter] = item.auth;

                var fi_img = "<img class='posterIcon' id='posterIcon"+ counter +"' src='"+define_icon+"'>"
                var fi_auth = "<div class='authBar'>"+ fi_img +"<label class='auth' id='auth"+counter+"'>Null</label></div>";
                var fi_date = "<label class='date'>"+ "#" + counter.toString() + " " + item.date + "</label>";
                var fi_content = "<label class='content' id='content"+counter+"'></label>";
                var fi = "<div class='block'>"+ fi_auth + fi_date + fi_content +"</div>";
        
                poster.append(fi);
                $("#content"+counter.toString()).text(item.content);
                $("#content"+counter.toString()).html($("#content"+counter.toString()).html().replace(/\n/g,"<br>"));
                counter++;
            })
            getAuthName(auth, 1, counter);
            getIcon();

        })

    })
}
function getAuthName(auth, i, counter){
    if(i<counter){
        var authRef = firebase.database().ref("user/profile").orderByChild("email").equalTo(auth[i]);
        authRef.once("value").then(authSnp=>{

            $.each(authSnp.val(),(i_, item_)=>{
                $("#auth"+i.toString()).text(item_.name);
                var photoStr = "img/" + item_.photo;
                var storageRef = firebase.storage().ref(photoStr);
    
                storageRef.getDownloadURL().then(url=>{
                    $("#posterIcon"+i).attr("src",url);
                    getAuthName(auth,i+1,counter);
                }).catch(()=>{
                    getAuthName(auth,i+1,counter);
                })
            })
            
        })
    }
}


function checkYourComment(email){
    var userRef = firebase.database().ref("user/profile").orderByChild("email").equalTo(email);
    userRef.once("value").then(snp=>{
        var key = Object.keys(snp.val())[0];
        $.each(snp.val(), (i,item)=>{
            $.each(item.poster, (j,j_item)=>{
                var note = new Notification("你的標題為 "+ j_item.title + "的貼文有新的回應");
            })
        })
        
        firebase.database().ref("user/profile/"+key+"/poster").remove();
    })
}

function loading(state){
    if(state == true){
        $("#loading").css("display","block");
    }else{
        $("#loading").css("display","none");
    }
}

function Logout() {
    firebase.auth().signOut().then(function () {})
}